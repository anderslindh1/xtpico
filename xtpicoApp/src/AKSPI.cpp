/*
 * AKSPI.cpp
 *
 *  Created on: Jul 11, 2019
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>

#include <asynPortDriver.h>
#include "AKSPI.h"

static const char *driverName = "AKSPI";

asynStatus AKSPI::pack(unsigned char type, unsigned char *data,
		unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char msg[AK_MAX_MSG_SZ] = {0};
	int i;
	int l = 0;

	// build the request
	for (i = 0; i < len; i++) {
		msg[l++] = *(data + i);		// data byte[i]
	}

	memset(mReq, 0, AK_MAX_MSG_SZ);
	memset(mResp, 0, AK_MAX_MSG_SZ);
	mReqActSz = 0;
	mRespActSz = 0;
	mReqSz = l;
	// maximum bytes received depend on the operation
	if (type == AK_REQ_TYPE_WRITE) {
		mRespSz = 0;
	} else if (type == AK_REQ_TYPE_READ) {
		mRespSz = len;
	}

	memcpy(mReq, msg, mReqSz);

	return status;
}

asynStatus AKSPI::unpack(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status) {

	if (mRespActSz == *len) {
		D(printf("OK!\n"));
		if ((type == AK_REQ_TYPE_READ) && data && len) {
			memcpy(data, &mResp, *len * sizeof(unsigned char));
		}
		status = asynSuccess;
		sprintf(mStatusMsg, "OK");
	} else {
		sprintf(mStatusMsg, "Invalid SPI response size received");
	}

	return status;
}

asynStatus AKSPI::xfer(int asynAddr, unsigned char type, unsigned char *data,
		unsigned short *len, double timeout) {
	asynStatus status = asynSuccess;

	if ((type != AK_REQ_TYPE_WRITE) && (type != AK_REQ_TYPE_READ)) {
		sprintf(mStatusMsg, "Invalid request type");
		status = asynError;
	}

	if (status == asynSuccess) {
		status = pack(type, data, *len);
	}
	if (status == asynSuccess) {
		if (type == AK_REQ_TYPE_WRITE) {
			/* No data is returned by the remote host */
			status = ipPortWrite(timeout);
		} else {
			/* One or more bytes are returned by the remote host */
			status = ipPortWriteRead(timeout);
			status = unpack(type, data, len, status);
		}
	}

	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s::%s, status=%s, message=%s\n",
			driverName, __func__, pasynManager->strStatus(status), mStatusMsg);
	}

	setStringParam(asynAddr, AKStatusMessage, mStatusMsg);
	char m[AK_MAX_MSG_SZ] = {0};
	getStringParam(asynAddr, AKStatusMessage, AK_MAX_MSG_SZ, m);
	D(printf("Status message: '%s'\n", m));

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(asynAddr, asynAddr);

	/* return error status of the previous access */
	return status;
}

void AKSPI::report(FILE *fp, int details) {

	fprintf(fp, "AKSPI %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKBase::report(fp, details);
}

/** Constructor for the AKSPI class.
  * Calls constructor for the AKBase base class.
  * All the arguments are simply passed to the AKBase base class.
  */
AKSPI::AKSPI(const char *portName, const char *ipPort,
		int interfaceMask, int interruptMask,
		int asynFlags, int autoConnect, int priority, int stackSize)
	: AKBase(portName,
		ipPort,
		AK_IP_PORT_SPI,
		1,
		interfaceMask,
		interruptMask,
		asynFlags, autoConnect, priority, stackSize)
{
	I(printf("init OK!\n"));
}

AKSPI::~AKSPI() {
	I(printf("shut down ..\n"));
}
