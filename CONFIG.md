# XT-PICO-SXL configuration

Following settings have been empricaly discovered.
Use web interface to navigate to appropriate menus and adjust the values.
Refer to `manual_xxl_ts.pdf` for details on how to do this.

## IP address

By default the XT pico has IP address `192.168.100.100` set.

If the network has DHCP server present, XT pico will try to (automatically)
obtain IP address from that DHCP server (no additional configuration is
required on XT pico side).

NOTE:
 At the moment ESS technical network has issues assigning DHCP address to the
 XT pico due to some DHCP configuration used.
 See https://jira.esss.lu.se/browse/INFRA-1116.

## TCP Port Timeout

Do not use, set to 0 (disabled).

The communication with the IOC is dropped after n seconds (30 by default).
This makes all the requests toward the XT pico fail with error in IOC is more
than n seconds have passed since the last request.

    =========================== TCP MENU ========================================

      1 = Port Timeout      = 0 (sec)
      2 = TCP-KeepAlive(Y/N)= N  (CheckLine)
      3 = Naglemode (Y/N)   = Y
      4 = Num. of TxPackets = 0 (auto)

## Port configuration

Make sure that the ports 1 & 2 are properly configured.
I2C needs to be first and uses TCP/IP port 1002, SPI needs to be second
and uses TCP/IP port 1003; these are the default port numbers and used
by the IOC startup as well.

    =========================== INTERFACE MENU ==================================

      1 = I2C1 Menu
      2 = SPI2 Menu
      E = ETHERNET Menu

### I2C

Use transparent mode (Data Control = P).

    =========================== I2C CONFIG MENU =================================

      1 = Slave Addr        = 0
      2 = Baudrate          = 100000
      3 = Data Control      = P
      4 = Data ready timeout= 10(*10ms)

      5 = Flow Control      = N
      6 = RTS Protocol      = 0

      a = Emulation         = TCPSERVER
      b = EmuCode           = 0000
      c = BUS               = I2C
      d = InputTimeOut*10ms = 0
      e = Local Port        = 1002
      f = With SSL/TLS      = N

### SPI

Set desired SPI clock and data bits. Configuration below is for BPM RFFE.
Clock polarity (CPOL) and clock phase (CPHA) might also need changing.

    =========================== SPI CONFIG MENU =================================

      1 = Master/Slave      = M
      2 = Bitrate           = 125000(125.000)
      3 = Databits          = 24
      4 = CPOL              = 0
      5 = CPHA              = 0
      6 = Data Control      = N
      7 = Data Poll*10ms    = 10
      8 = Flow Control      = N
      9 = RTS Protocol      = 0
      a = CS Control        = S

      b = Emulation         = TCPSERVER
      c = EmuCode           = 0000
      d = BUS               = SPI
      e = InputTimeOut*10ms = 0
      f = Local Port        = 1003
      g = With SSL/TLS      = N

Note that the SPI bus is capable of talking to a single SPI slave - only
one chip select line is available on the XT pico. If more slaves are expected
on the same SPI bus external logic is required to route the chip select to
the desired SPI slave.
