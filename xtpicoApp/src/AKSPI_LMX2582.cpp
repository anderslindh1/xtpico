/*
 * AKSPI_LMX2582.cpp
 *
 *  Created on: Jul 11, 2019
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include "AKSPI_LMX2582.h"
#include "AKSPI_LMX2582_config.h"

static const char *driverName = "AKSPI_LMX2582";


static void exitHandler(void *drvPvt) {
	AKSPI_LMX2582 *pPvt = (AKSPI_LMX2582 *)drvPvt;
	delete pPvt;
}

asynStatus AKSPI_LMX2582::writeAll(int addr) {
	asynStatus status = asynSuccess;
	unsigned char data[3] = {0};
	unsigned short len;
	struct lmx2582_register *regs = NULL;
	int regconf;

	/* determine the selected register set */
	getIntegerParam(AKSPI_LMX2582_RegConfig, &regconf);
	if (! regconf) {
		D(printf("USING 352 MHz config\n"));
		regs = lmx2582_regs_352mhz;
	} else {
		D(printf("USING 704 MHz config\n"));
		regs = lmx2582_regs_704mhz;
	}

	len = 3;
	for (int i = 0; i < AKSPI_LMX2582_NR_REGS; i++) {
		/* setup address and 2 data bytes */
		data[0] = regs[i].addr;
		data[1] = regs[i].high;
		data[2] = regs[i].low;

		/* set the desired function of the MUXOut pin */
		if (regs[i].addr == AKSPI_LMX2582_R0_REG) {
			int muxoutsel;
			getIntegerParam(AKSPI_LMX2582_MUXOutSel, &muxoutsel);
			if (muxoutsel) {
				/* lock detect */
				data[2] |= (1 << AKSPI_LMX2582_R0_MUXOUT_SEL_BIT);
			} else {
				/* SPI readback */
				data[2] &= ~(1 << AKSPI_LMX2582_R0_MUXOUT_SEL_BIT);
			}
		}

		D(printf("WRITE [%02d] 0x%02X 0x%02X\n", data[0], data[1], data[2]));
		status = xfer(addr, AK_REQ_TYPE_WRITE, data, &len);
		if (status) {
			return status;
		}
	}

	// XXX workaround for last byte not being sent to SPI wire..
	//     remove once XT pico is fixed..
	data[0] = 0x80;
	data[1] = 0x00;
	data[2] = 0x00;
	D(printf("DUMMY READ [%02d] 0x%02X 0x%02X\n", data[0], data[1], data[2]));
	status = xfer(addr, AK_REQ_TYPE_WRITE, data, &len);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKSPI_LMX2582::readAll(int addr) {
	asynStatus status = asynSuccess;
	unsigned char data[3] = {0};
	unsigned short len;
	struct lmx2582_register *regs = NULL;
	int regconf;

	/* determine the selected register set */
	getIntegerParam(AKSPI_LMX2582_RegConfig, &regconf);
	if (! regconf) {
		regs = lmx2582_regs_352mhz;
	} else {
		regs = lmx2582_regs_704mhz;
	}

	len = 3;
	for (int i = 0; i < AKSPI_LMX2582_NR_REGS; i++) {
		/* setup address, OR'ed with 0x80 and 2 dummy data bytes */
		data[0] = regs[i].addr | AKSPI_LMX2582_READ_ACCESS;
		data[1] = 0xFF;
		data[2] = 0xFF;
		status = xfer(addr, AK_REQ_TYPE_READ, data, &len);
		if (status) {
			return status;
		}

		// first returned byte is always 0x00,
		// next 2 bytes is register value
		D(printf("READ [%02d] 0x%02X 0x%02X\n", regs[i].addr, mResp[1], mResp[2]));
	}

	// XXX workaround for last byte not being sent to SPI wire..
	//     remove once XT pico is fixed..
	data[0] = 0x80;
	data[1] = 0x00;
	data[2] = 0x00;
	D(printf("DUMMY READ [%02d] 0x%02X 0x%02X\n", data[0], data[1], data[2]));
	status = xfer(addr, AK_REQ_TYPE_WRITE, data, &len);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKSPI_LMX2582::writeInt32(asynUser *pasynUser, epicsInt32 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %d\n", function, addr, value));
	status = setIntegerParam(addr, function, value);

	if (function == AKSPI_LMX2582_WriteAll) {
		status = writeAll(addr);
	} else if (function == AKSPI_LMX2582_ReadAll) {
		status = readAll(addr);
	} else if (function < FIRST_AKSPI_LMX2582_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKSPI::writeInt32(pasynUser, value);
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%d\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

void AKSPI_LMX2582::report(FILE *fp, int details) {

	fprintf(fp, "AKSPI_LMX2582 %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKSPI::report(fp, details);
}

/** Constructor for the AKSPI_LMX2582 class.
  * Calls constructor for the AKSPI base class.
  * All the arguments are simply passed to the AKSPI base class.
  */
AKSPI_LMX2582::AKSPI_LMX2582(const char *portName, const char *ipPort,
		int priority, int stackSize)
	: AKSPI(portName,
		ipPort,
		0, /* no new interface masks beyond those in AKBase */
		0, /* no new interrupt masks beyond those in AKBase */
		ASYN_CANBLOCK | ASYN_MULTIDEVICE,
		1, /* autoConnect YES */
		priority, stackSize)
{
	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	createParam(AKSPI_LMX2582_WriteAllString,	asynParamInt32,	&AKSPI_LMX2582_WriteAll);
	createParam(AKSPI_LMX2582_ReadAllString,	asynParamInt32,	&AKSPI_LMX2582_ReadAll);
	createParam(AKSPI_LMX2582_MUXOutSelString,	asynParamInt32,	&AKSPI_LMX2582_MUXOutSel);
	createParam(AKSPI_LMX2582_RegConfigString,	asynParamInt32,	&AKSPI_LMX2582_RegConfig);

	I(printf("init OK!\n"));
}

AKSPI_LMX2582::~AKSPI_LMX2582() {
	I(printf("shut down ..\n"));
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int AKSPILMX2582Configure(const char *portName, const char *ipPort,
		int priority, int stackSize) {
	new AKSPI_LMX2582(portName, ipPort, priority, stackSize);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",		iocshArgString};
static const iocshArg initArg1 = { "ipPort",		iocshArgString};
static const iocshArg initArg2 = { "priority",		iocshArgInt};
static const iocshArg initArg3 = { "stackSize",		iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
											&initArg1,
											&initArg2,
											&initArg3};
static const iocshFuncDef initFuncDef = {"AKSPILMX2582Configure", 4, initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	AKSPILMX2582Configure(args[0].sval, args[1].sval,
			args[2].ival, args[3].ival);
}

void AKSPILMX2582Register(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(AKSPILMX2582Register);

} /* extern "C" */
