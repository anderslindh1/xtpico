# XT-PICO-SXL (from AK-NORD)

This repository holds EPICS support for XT-PICO-SXL module from AK-NORD.
Tested with EVA KIT development board from AK-NORD and ESS Ethernet module board.

See _docs_ folder for chip PDF manuals and board schematics.

See also vendor information:

* http://www.ak-nord.de
* http://www.ak-nord.de/en/daten/manual_xxl_ts.pdf
* http://www.ak-nord.de/de/daten/handbuch_xxl_ts.pdf

## Supported chips

List of I2C/SPI chips supported by this EPICS module:

* Linear LTC2991 voltage / temperature monitor
* TI TMP100 temperature sensor
* TI TCA9555 port expander
* ST M24M02 EEPROM
* TI LMX2582 VCO/PLL
* Analog devices HMC624A digital attenuator
* Analog devices AD527x digital resistor
* Analog Devices ADT7420 temperature sensor
* Maxim DS28CM00 ID number
* NXP PCF85063TP real-time clock / calendar
* TI TCA9546A I2C/SmBus multiplexer (switch)

## XT-PICO-SXL configuration

For general configuration see CONFIG.md.

Configuration interface is accessible via web browser, at *http://192.168.1.100* (default IP.

## EPICS module

TBD

## Sample OPI

![Screenshot_2017-04-11_13-43-11.png](https://bitbucket.org/repo/rpKLkag/images/606404051-Screenshot_2017-04-11_13-43-11.png)

## E3 module
This module can also be compiled as an E3 module:
* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require asyn`
* Run `make -f Makefile.E3 target` to build `target` using the E3 build process
