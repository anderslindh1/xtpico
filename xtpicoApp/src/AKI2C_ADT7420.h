/*
 * AKI2C_ADT7420.h
 *
 *  Created on: Jul 18, 2019
 *      Author: hinkokocevar
 */

#ifndef _I2C_ADT7420_H_
#define _I2C_ADT7420_H_

#include "AKI2C.h"

#define AKI2C_ADT7420_TEMPERATURE_REG			0x00
#define AKI2C_ADT7420_CONFIG_REG				0x03

#define AKI2C_ADT7420_RESOLUTION_SHIFT			7
#define AKI2C_ADT7420_RESOLUTION_13BIT			0
#define AKI2C_ADT7420_RESOLUTION_16BIT			1

#define AKI2C_ADT7420_ValueString				"AKI2C_ADT7420_VALUE"
#define AKI2C_ADT7420_ReadString				"AKI2C_ADT7420_READ"
#define AKI2C_ADT7420_ResolutionString			"AKI2C_ADT7420_RESOLUTION"

/*
 * Chip			: Analog Devices ADT7420
 * Function		: temperature sensor
 * Bus			: I2C
 * Access		: TCP/IP socket on AK-NORD XT-PICO-SX
 */
class AKI2C_ADT7420: public AKI2C {
public:
	AKI2C_ADT7420(const char *portName, const char *ipPort,
			int devCount, const char *devInfos, int priority, int stackSize);
	virtual ~AKI2C_ADT7420();

	/* These are the methods that we override from AKI2C */
	virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* Our parameter list */
	int AKI2C_ADT7420_Read;
#define FIRST_AKI2C_ADT7420_PARAM AKI2C_ADT7420_Read
	int AKI2C_ADT7420_Value;
	int AKI2C_ADT7420_Resolution;

private:
	asynStatus write(int addr, unsigned char reg, unsigned short val, unsigned short len);
	asynStatus read(int addr, unsigned char reg, unsigned short *val, unsigned short len);
	asynStatus readTemperature(int addr);
	asynStatus writeResolution(int addr, unsigned short val);
};

#endif /* _I2C_ADT7420_H_ */
