/*
 * AKSPI.h
 *
 *  Created on: Jul 11, 2019
 *      Author: hinkokocevar
 */

#ifndef _AKSPI_H_
#define _AKSPI_H_

#include "AKBase.h"

/** Driver for AK-NORD XT-PICO-SXL TTL IO bus access over TCP/IP socket */
class AKSPI: public AKBase {
public:
	AKSPI(const char *portName, const char *ipPort,
		int interfaceMask, int interruptMask,
		int asynFlags, int autoConnect, int priority, int stackSize);
	virtual ~AKSPI();

	/* These are the methods that we override from AKBase */
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* These are new methods */
	asynStatus xfer(int asynAddr, unsigned char type, unsigned char *data,
			unsigned short *len, double timeout = 0.3);

	/* No parameter list */

private:
	/* These are new methods */
	asynStatus pack(unsigned char type, unsigned char *data, unsigned short len);
	asynStatus unpack(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status);
};

#endif /* _AKSPI_H_ */
